import os

import pandas as pd
import numpy as np
import datetime
__author__ = 'lotan'

ONE_WEEK = 604800
DECAY_CONST = -np.log(0.9) / ONE_WEEK

base_path = os.path.dirname(__file__)
FEATURES_FILE = os.path.join(base_path, 'ChicaGo_RowData_2011_2015_with_features.csv')


def get_month(row):
    if row['Month_Apr'] == 1:
        return 4
    elif row['Month_Aug'] == 1:
        return 8
    elif row['Month_Dec'] == 1:
        return 12
    elif row['Month_Feb'] == 1:
        return 2
    elif row['Month_Jan'] == 1:
        return 1
    elif row['Month_Jul'] == 1:
        return 7
    elif row['Month_Jun'] == 1:
        return 6
    elif row['Month_Mar'] == 1:
        return 3
    elif row['Month_May'] == 1:
        return 5
    elif row['Month_Nov'] == 1:
        return 11
    elif row['Month_Oct'] == 1:
        return 10
    elif row['Month_Sep'] == 1:
        return 9
    else:
        raise ValueError('error in get_month(). couldn''t parse the month in this row: ' + row)


def get_hour(row):
    if row['BinHours_0-3'] == 1:
        return 0
    elif row['BinHours_12-15'] == 1:
        return 12
    elif row['BinHours_15-18'] == 1:
        return 15
    elif row['BinHours_18-21'] == 1:
        return 18
    elif row['BinHours_21-24'] == 1:
        return 21
    elif row['BinHours_3-6'] == 1:
        return 3
    elif row['BinHours_6-9'] == 1:
        return 6
    elif row['BinHours_9-12'] == 1:
        return 9
    else:
        raise ValueError('error in get_hour(). couldn''t parse the hour of day in this row: ' + row)


def get_decay(now, row):
    rowtime = datetime.datetime(year=2000 + int(row['YearNum']), month=get_month(row), day=int(row['DayOfMonth']),
                                hour=get_hour(row))
    delta = (rowtime - now).total_seconds()

    decay = np.exp(delta * DECAY_CONST)
    return decay


def score_block(block, now, df):
    sum = 0
    rows = df[df.Block == block]
    for i in range(len(rows)):
        row = rows.iloc[i, :]
        decay = get_decay(now, row)
        points = row['CrimesCount'] * decay
        sum += points
    return sum


def score_blocks(blocks, now, df):
    sum = 0
    for block in blocks:
        sum += score_block(block, now, df)
    return sum


def train_and_test_model(features_file):
    df = pd.read_csv(features_file)
    return df


if __name__ == '__main__':
    print 'loading features file ' + FEATURES_FILE
    df = train_and_test_model(FEATURES_FILE)

    d = datetime.datetime(year=2015, month=11, day=20, hour=12)
    blocks = ['0000X E 100TH PL', '0000X E 103RD ST', '0000X E 117TH PL']
    score = score_blocks(blocks, d, df)

    print 'd == ' + str(d)
    print 'blocks == ' + str(blocks)
    print 'score == ' + str(score)
    print ''

    d = datetime.datetime(year=2015, month=11, day=10, hour=12)
    blocks = ['0000X E 100TH PL', '0000X E 103RD ST', '0000X E 117TH PL']
    score = score_blocks(blocks, d, df)

    print 'd == ' + str(d)
    print 'blocks == ' + str(blocks)
    print 'score == ' + str(score)
    print ''

    print 'done :)'
