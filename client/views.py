import logging

from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse
from funcy.simple_funcs import rpartial
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from chicaGo.gis import route , models
from .models import Greeting


logger = logging.getLogger(__name__)


def index(request):
    return render(request, 'index.html', context={'key': settings.MAP_QUEST_CONSUMER_KEY})


def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})


class RoutesApi(APIView):
    def get(self, request):
        start, end = request.query_params['start'], request.query_params['end']
        logger.info('Calculating paths from {} to {}'.format(start, end))
        possible_routes = route.get_routes(start, end)
        routes_and_blocks = dict()
        routes_crimes_and_score = dict()
        for pr in possible_routes:
            routes_and_blocks[pr] = route.get_crimes_around_route(pr,10)
            routes_crimes_and_score[pr] = models.get_route_score(routes_and_blocks[pr],None)

        return Response([{ "geojson" : r.geojson(),
                           "crimes" : routes_crimes_and_score[r][0],
                           "score" : routes_crimes_and_score[r][1]
                           } for r in possible_routes])
