/**
 * Created on 11/26/15.
 */


var module = angular.module('chicago');

module.controller('BaseCtrl', [
    'lodash',
    '$scope',
    '$q',
    'uiGmapGoogleMapApi',
    'uiGmapIsReady',
    'RoutesAPI',
    function (_, $scope, $q, uiGmapGoogleMapApi, uiGmapIsReady, RoutesAPI) {

        var maps, directionsRenderer, map, markers = [], bounds, geocoder, autocomplete;
        $scope.mapApi = {};

        var apiPromise = uiGmapGoogleMapApi.then(function(mapsResponse) {
            return mapsResponse
        });
        var mapPromise = uiGmapIsReady.promise();

        $q.all([apiPromise, mapPromise]).then(function (api_instances) {
            var api = api_instances[0], instances = api_instances[1];
            maps = api;
            map = instances[0].map;
            directionsRenderer = new maps.DirectionsRenderer();
            directionsRenderer.setMap(map);
            bounds = new maps.LatLngBounds();
            geocoder = new maps.Geocoder();
        });

        function updateLocation(marker) {
            geocoder.geocode({'location': marker.position}, function(result, status) {
                if (status === maps.GeocoderStatus.OK && result[1]) {
                    if (marker.label === 'S') {
                        $scope.start = result[1].formatted_address
                    } else {
                        $scope.end = result[1].formatted_address
                    }
                }
            })
        }

        function placeMarker(latLng, map, previousMarkers) {
            if (previousMarkers.length < 2) {
                var marker = new maps.Marker({
                    position: latLng,
                    map: map,
                    label: !(previousMarkers.length % 2) ? 'S' : 'E'
                });
                marker.addListener('click', function (e) {
                    marker.setMap(null);
                    _.pull(previousMarkers, marker);
                });
                previousMarkers.push(marker);
                updateLocation(marker);
                _.each(_.pluck(previousMarkers, 'position'), function (pos) { bounds.extend(pos)});
            }
        }

        function cleanRoutes(routes) {
            _.each(routes, function (route) {
                route.setMap(null);
            });
            _.each(crimes, function (crime) {
                crime.setMap(null)
            })
        }

        var colours = [
            '#1C91FF',
            '#721AD1',
            '#FFDD00',
            '#DE0707'
        ];
        var displayedRoutes = [], crimes = [];
        function displayRoutes(response) {
            routesPromise = null;
            function toLngLat(tuple) {
                return {lng: tuple[0], lat: tuple[1]}
            }
            var routes = response.data;
            routes = _.sortBy(routes, 'score');
            var colourIdx = 0;
            _.each(routes, function(route) {
                var polyline = new maps.Polyline({
                    path: _.map(_.find(_.pluck(route.geojson.features, 'geometry'), {type: 'LineString'}).coordinates, toLngLat),
                    geodesic: false,
                    strokeColor: colours[colourIdx < colours.length ? colourIdx++ : colours.length - 1],
                    strokeOpacity: 0.8,
                    strokeWeight: 4
                });
                displayedRoutes.push(polyline);
                polyline.setMap(map);
            });
            _.each(routes, function (route) {
                _.each(route.crimes, function (crime) {
                    var crimeCircle = new maps.Circle({
                        fillColor: '#DE0707',
                        fillOpacity: .4,
                        radius: 15,
                        strokeColor: 'white',
                        strokeWeight: 1,
                        center: {lng: crime.point[0][0], lat: crime.point[1][0] }
                    });
                    crimeCircle.setMap(map);
                    crimes.push(crimeCircle);
                })
            })
        }

        var routesPromise = null;

        $scope.isWaiting = function () {
            return !!routesPromise
        };

        function getPathIfNeeded(markers) {
            if (markers.length == 2) {
                var start = _.find(markers, {label: 'S'}), end = _.find(markers, {label: 'E'});
                routesPromise = RoutesAPI.getRoutes(start, end);
                routesPromise.then(displayRoutes);
                cleanRoutes(displayedRoutes);
                map.setCenter(bounds.getCenter());
                map.fitBounds(bounds);
            }
        }

        $scope.mapEvents = {
            click: function (map, eventName, eventData) {
                placeMarker(eventData[0].latLng, map, markers);
                getPathIfNeeded(markers)
            }
        };

        $scope.map = { center: { latitude: 41.87518, longitude: -87.732696 }, zoom: 12 };
    }
]);
